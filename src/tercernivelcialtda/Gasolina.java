/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tercernivelcialtda;

/**
 *
 * @author USUARIO
 */
public class Gasolina {
    
    private String Tipo;
    private double CantidadGalones;
    private double PrecioGalon;
    private double IVA;

    public Gasolina() {
        setIVA();
    }
    
    

    public String getTipo() {
        return Tipo;
    }

    public void setTipo(String Tipo) {
        this.Tipo = Tipo;
    }

    public double getCantidadGalones() {
        return CantidadGalones;
    }

    public void setCantidadGalones(double CantidadGalones) {
        this.CantidadGalones = CantidadGalones;
    }

    public double getPrecioGalon() {
        return PrecioGalon;
    }

    public double getIVA() {
        return IVA;
    }

    public void setIVA() {
        this.IVA = 12;
    }
    
    public double CalcularTotal()
    {
        if (this.Tipo == "Extra")
        {
            this.PrecioGalon = 1.5;
        }
        else{
            this.PrecioGalon = 2.0;
        }
        double ivaCalculado = CalcularSubtotal() * (this.IVA/100);
        return CalcularSubtotal() + ivaCalculado;
    }
    
    private double CalcularSubtotal()
    {
        return (this.CantidadGalones * this.PrecioGalon);
    }
    
}
